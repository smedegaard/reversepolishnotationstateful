defmodule ReversePolishNotationStateful do
  use GenServer

  # Client API
  def start_link(opts \\ []) do
    GenServer.start_link(__MODULE__, opts, name: __MODULE__)
  end

  def stop() do
    GenServer.stop(__MODULE__)
  end

  def push(item) do
    GenServer.call(__MODULE__, {:push, item})
  end

  def push() do
    GenServer.call(__MODULE__, :push)
  end

  def peek() do
    GenServer.call(__MODULE__, :peek)
  end

  def reset() do
    GenServer.cast(__MODULE__, :reset)
  end

  # Server callbacks
  @impl true
  def init(_opts) do
    {:ok, []}
  end

  @impl true
  def handle_cast(:reset, _state) do
    {:noreply, []}
  end

  @impl true
  def handle_call({:push, number}, _from, state) when is_number(number) do
    {:reply, [number | state], [number | state]}
  end

  @impl true
  def handle_call(:push, _from, state) do
    {:reply, state, state}
  end

  def handle_call({:push, :/}, _from, [_x, second | _list] = state) when second == 0 do
    response = {:error, :division_by_zero}
    {:reply, response, state}
  end

  @operators [:+, :/, :-, :*]
  def handle_call({:push, op}, _from, [x, y | list]) when op in @operators do
    result = apply(Kernel, op, [x, y])
    new_state = [result | list]
    {:reply, new_state, new_state}
  end

  def handle_call({:push, otherwise}, _from, state) do
    response = {:error, {:unexpected_input, otherwise}}
    {:reply, response, state}
  end

  def handle_call(:peek, _from, state) do
    {:reply, state, state}
  end
end
