defmodule ReversePolishNotationStatefulOperationsTest do
  use ExUnit.Case
  use PropCheck
  use PropCheck.StateM

  #################################
  ##                             ##
  ##   SWITCH TO `fail` branch   ##
  ##                             ##
  #################################

  @operators [:/, :*, :-, :+]

  require Logger

  property "Reverse Polish Notation operations", [:verbose, numtests: 1000] do
    forall cmds <- commands(__MODULE__) do
      ReversePolishNotationStateful.start_link()

      {history, state, result} = run_commands(__MODULE__, cmds)

      ReversePolishNotationStateful.stop()

      (result == :ok)
      |> aggregate(command_names(cmds))
      |> when_fail(
        IO.puts("""
        History: #{inspect(history)}
        State: #{inspect(state)}
        Result: #{inspect(result)}
        """)
      )
    end
  end

  ###########
  ## State ##
  ###########

  def initial_state(), do: []

  #############
  ## Command ##
  #############

  def command(state) do
    cond do
      Enum.count(state) > 2 ->
        frequency([
          {9, {:call, ReversePolishNotationStateful, :push, [number()]}},
          {3, {:call, ReversePolishNotationStateful, :push, [operator()]}},
          {1, {:call, ReversePolishNotationStateful, :peek, []}}
        ])

      true ->
        frequency([
          {9, {:call, ReversePolishNotationStateful, :push, [number()]}},
          {1, {:call, ReversePolishNotationStateful, :peek, []}}
        ])
    end
  end

  ##################################################
  ##             Preconditions                    ##
  ## - Decides if the command is valid.           ##
  ## - Discards the command if `false`is returned ##
  ##################################################

  # Cannot push operators if the state has less than 2 elements
  def precondition(state, {:call, ReversePolishNotationStateful, :push, [arg]})
      when is_atom(arg) do
    Enum.count(state) > 2
  end

  def precondition(_state, {:call, ReversePolishNotationStateful, _function, _args}) do
    true
  end

  #########################################################
  ##                  POST CONDITIONS                    ##
  ## - Validating that the state of the acctual system   ##
  ##   matches the symbolic state                        ##
  #########################################################

  # When calling `peek\0` the state should not change
  def postcondition(
        state,
        {:call, ReversePolishNotationStateful, :peek, []},
        result
      ) do
    state == result
  end

  # Division by zero results in an error tuple
  def postcondition(
        [_first, second | _tail] = _state,
        {:call, ReversePolishNotationStateful, :push, [:/]},
        result
      )
      when second == 0 do
    {:error, :division_by_zero} == result
  end

  # Pushing a number prepends the number to the stack
  def postcondition(
        state,
        {:call, ReversePolishNotationStateful, :push, [arg]},
        _result = [_head | tail]
      )
      when is_number(arg) do
    state == tail
  end

  # GIVEN that you push an operator to the stack
  # WHEN there is 2 or more numbers in the stack
  # THEN the 2 first numbers are poped
  # AND the operator is appiled to the poped numbers
  # AND the result is pushed to the stack
  def postcondition(
        state,
        {:call, ReversePolishNotationStateful, :push, [arg]},
        result
      )
      when is_atom(arg) do
    [x, y | state_tail] = state

    result == [apply(Kernel, arg, [x, y]) | state_tail]
  end

  ####################################
  ##         NEXT STATE             ##
  ##   - How is the state updated?  ##
  ##   - If at all...               ##
  ####################################

  # Calling `peek\0` yeilds the same state
  def next_state(
        state,
        _result,
        {:call, ReversePolishNotationStateful, :peek, []}
      ) do
    state
  end

  # Errors yeilds the same state
  def next_state(
        state,
        _result = {:error, _reason},
        {:call, ReversePolishNotationStateful, :push, _args}
      ) do
    state
  end

  # Division by zero yeilds the same state
  def next_state(
        [_first, second | _tail] = state,
        _result,
        {:call, ReversePolishNotationStateful, :push, [:/]}
      )
      when second == 0 do
    state
  end

  ## pushing a number prepends it to the state
  def next_state(state, _result, {:call, ReversePolishNotationStateful, :push, [arg]})
      when is_number(arg) do
    new_state = [arg | state]
    new_state
  end

  # Pushing an operator when there is less than two numbers yeilds same state
  def next_state(state, _result, {:call, ReversePolishNotationStateful, :push, [arg]})
      when length(state) < 2 do
    case Enum.member?(@operators, arg) do
      true -> state
      false -> [arg | state]
    end
  end

  # When an operator is pushed and there is two or more numbers in the stack
  # the operator is applied to the two first numbers
  def next_state(
        [first, second | tail] = state,
        _result,
        {:call, ReversePolishNotationStateful, :push, [arg]}
      )
      when is_atom(arg) do
    # IO.puts("""
    # #{inspect(arg)}
    # #{inspect(state)}
    # """)

    case Enum.member?(@operators, arg) do
      true ->
        [apply(Kernel, arg, [first, second]) | tail]

      false ->
        [arg | state]
    end
  end

  ################
  ## Generators ##
  ################

  def operator() do
    oneof(@operators)
  end
end
